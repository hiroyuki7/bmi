/**
 * BMIを測定するWebアプリ
 * knockout.js + bootstrap
 * @author Kawai Hiroyuki
 * @version 1.0.0 2017.08.02
 */
window.onload = function () {

    function AppViewModel() {
        var self = this;

        self.height = ko.observable("0");
        self.weight = ko.observable("0");
        self.bmi = ko.computed(function() {
            var heightM = self.height() / 100;
            var score = (self.weight() / (heightM * heightM));
            console.log("bmi score:" + score);
            // http://qiita.com/south37/items/e400a3a698957ab4aa7a
            if (isNaN(score)) {
                return 0;
            }
            return score;
        });
    }

    // Activates knockout.js
    ko.applyBindings(new AppViewModel());
};